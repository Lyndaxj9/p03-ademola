//
//  GOView.h
//  p03-ademola
//
//  Created by Lynda on 2/26/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GOView : UIView
@property (nonatomic, strong) IBOutlet UIView *contents;
@property (nonatomic, strong) IBOutlet UILabel *message;
@property (nonatomic, strong) IBOutlet UIButton *tryAgain;
@property (nonatomic, strong) IBOutlet UIButton *quit;

@end
