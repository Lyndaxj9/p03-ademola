//
//  StartController.m
//  p03-ademola
//
//  Created by Lynda on 2/26/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import "StartController.h"
#import "ViewController.h"

@interface StartController ()

@end

@implementation StartController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonClick {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ViewController *sController = [storyboard instantiateViewControllerWithIdentifier:@"ViewControl"];
    [self presentViewController:sController animated:YES completion:NULL];
    NSLog(@"buttonClicked");
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
