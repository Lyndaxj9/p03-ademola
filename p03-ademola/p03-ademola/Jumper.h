//
//  Jumper.h
//  p03-ademola
//
//  Created by Lynda on 2/14/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Jumper : UIView
@property (nonatomic) float dx, dy; //velocity
@end
