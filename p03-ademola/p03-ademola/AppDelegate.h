//
//  AppDelegate.h
//  p03-ademola
//
//  Created by Lynda on 2/14/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

