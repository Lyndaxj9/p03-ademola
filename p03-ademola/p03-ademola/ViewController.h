//
//  ViewController.h
//  p03-ademola
//
//  Created by Lynda on 2/14/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameView.h"
#import "StartController.h"

@interface ViewController : UIViewController
@property (nonatomic, strong) IBOutlet GameView *gameView;

-(void)mainScreen;
@end

