//
//  ViewController.m
//  p03-ademola
//
//  Created by Lynda on 2/14/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (nonatomic, strong) CADisplayLink *displayLink;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _displayLink = [CADisplayLink displayLinkWithTarget:_gameView selector:@selector(arrange:)];
    [_displayLink setPreferredFramesPerSecond:60];
    [_displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- () have method to control the slider thus jumper here
//- since i don't want to use tilt I have to do something different
-(IBAction)speedChange:(id)sender
{
    UISlider *s = (UISlider *)sender;
    //NSLog(@"tilt %f", (float)[s value]);
    [_gameView setTilt:(float)[s value]];
}

- (void)mainScreen {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    StartController *sController = [storyboard instantiateViewControllerWithIdentifier:@"StartControl"];
    [self presentViewController:sController animated:YES completion:NULL];
    NSLog(@"buttonClicked");
}



@end
