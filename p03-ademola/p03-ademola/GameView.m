//
//  GameView.m
//  p03-ademola
//
//  Created by Lynda on 2/14/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import "GameView.h"

@implementation GameView
@synthesize jumper, bricks, levelEnd;
@synthesize messageWindow;
@synthesize tilt, preTilt;
@synthesize maxDistanceY, minDistanceY;
@synthesize jumped, endSighted, gameOver;


//Initalize the game screen with jumper and bricks
-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        CGRect bounds = [self bounds];
        
        jumper = [[JumperI alloc] initWithFrame:CGRectMake(bounds.size.width/2, bounds.size.height - 20, 30, 30)];
        jumper.image = [UIImage imageNamed:@"JumperI.png"];
        [jumper setDx:0];
        [jumper setDy:10];
        [self addSubview:jumper];
        
        /*
        jumper = [[Jumper alloc] initWithFrame:CGRectMake(bounds.size.width/2, bounds.size.height - 20, 20, 20)];
        [jumper setBackgroundColor:[UIColor redColor]];
        [jumper setDx:0];
        [jumper setDy:10];
        [self addSubview:jumper];
         */
        preTilt = 0;
        maxDistanceY = 145;
        minDistanceY = 60;
        jumped = false;
        endSighted = false;
        gameOver = false;
        [self makeBricks:nil];
    }
    return self;
}

-(void)makeBricks:(id)sender
{
    CGRect bounds = [self bounds];
    NSLog(@"bounds width: %f | height: %f", bounds.size.width, bounds.size.height);
    float width = bounds.size.width * .2;
    float height = 25;
    float xPoint, yPoint;
    
    if (bricks)
    {
        for (Brick *brick in bricks)
        {
            [brick removeFromSuperview];
        }
    }
    
    bricks = [[NSMutableArray alloc] init];
    
    Brick *b = [[Brick alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    [b setBackgroundColor:[UIColor redColor]];
    [self addSubview:b];
    
    xPoint = arc4random() % (int)(bounds.size.width * .8);
    
    //NSLog(@"generated yPoint: %f", yPoint);
    NSLog(@"generated xPoint: %f", xPoint);
    yPoint = bounds.size.height - 70;
    xPoint = bounds.size.width * .2;
    NSLog(@"actual xPoint: %f | yPoint: %f", xPoint, yPoint);
    
    [b setCenter:CGPointMake(xPoint, yPoint)];
    [bricks addObject:b];
    
    for (int i = 1; i < 4; ++i)
    {
        Brick *b = [[Brick alloc] initWithFrame:CGRectMake(0, 0, width, height)];
        [b setBackgroundColor:[UIColor blueColor]];
        [self addSubview:b];
        
        xPoint = 40 + arc4random() % (int)(bounds.size.width * .6);
        yPoint = arc4random() % (int)(bounds.size.height * .8);
        NSLog(@"generated xPoint: %f | yPoint: %f", xPoint, yPoint);
        CGPoint p = [bricks[i-1] center];
        NSLog(@"block %d p.y %f", i-1, p.y);
        if(yPoint < p.y - maxDistanceY || yPoint > p.y - minDistanceY){
            yPoint = p.y - maxDistanceY;
            
        }
        NSLog(@"actual yPoint: %f", yPoint);
        [b setCenter:CGPointMake(xPoint, yPoint)];
        [bricks addObject:b];
    }
    
    for (int i = 4; i < 20; ++i) //i < 20
    {
        Brick *b = [[Brick alloc] initWithFrame:CGRectMake(0, 0, width, height)];
        [b setBackgroundColor:[UIColor greenColor]];
        [self addSubview:b];
        
        xPoint = 40 + arc4random() % (int)(bounds.size.width * .6);
        yPoint = -1 * arc4random() % (int)(bounds.size.height * .8);
        NSLog(@"generated off yPoint: %f", yPoint);
        CGPoint p = [bricks[i-1] center];
        NSLog(@"block %d p.y %f", i-1, p.y);
        if(yPoint < p.y - maxDistanceY || yPoint > p.y - minDistanceY){
            yPoint = p.y - maxDistanceY;
            
        }
        NSLog(@"actual yPoint: %f", yPoint);
        [b setCenter:CGPointMake(xPoint, yPoint)];
        [bricks addObject:b];
    }
    
    float endPosY = yPoint - maxDistanceY/2;
    float endPosX = bounds.size.width/2;
    levelEnd = [[EndBrick alloc] initWithFrame:CGRectMake(0, 0, bounds.size.width, 50)];
    [levelEnd setWidth:bounds.size.width];
    [levelEnd setHeight:50];
    [levelEnd setBackgroundColor:[UIColor orangeColor]];
    [self addSubview:levelEnd];
    [levelEnd setHidden:YES];
    
    [levelEnd setCenter:CGPointMake(endPosX, endPosY)];
}

-(void)arrange:(CADisplayLink *)sender
{
    //CFTimeInterval ts = [sender timestamp];
    
    CGRect bounds = [self bounds];
    //NSLog(@"arrange was called");
    // Apply gravity to the acceleration of the jumper
    [jumper setDy:[jumper dy] - .3];
    
    // Jumper moves in the direction of gravity
    CGPoint p = [jumper center];
    
    // Apply the tilt.  Limit maximum tilt to + or - 5
    //NSLog(@"Tilt was %f", tilt);
    //still doesn't work the way intended
    //if(preTilt + 0.1 <= tilt || preTilt - 0.1 >= tilt) {
    if(tilt > 0.1 || tilt < -0.1) {
        [jumper setDx:[jumper dx] + tilt];
        if ([jumper dx] > 5)
            [jumper setDx:5];
        if ([jumper dx] < -5)
            [jumper setDx:-5];
        //p.x += [jumper dx];
        
        //preTilt = tilt;
    } else {
        [jumper setDx:0];
    }
  
    if(gameOver){
        [jumper setDy:0];
        [jumper setDx:0];
    }
    // Jumper moves in the direction of gravity
    p.x += [jumper dx];
    p.y -= [jumper dy];
    
    // If the jumper has fallen below the bottom of the screen,
    // add a positive velocity to move him
    if (p.y > bounds.size.height)
    {
        if(jumped && !gameOver){
            NSLog(@"GAMEOVER");
            messageWindow = [[GOView alloc] initWithFrame:CGRectMake(bounds.size.width/2 - 250/2, bounds.size.height/2 - 175/2, 250, 175)];
            [messageWindow.tryAgain addTarget:self action:@selector(restartGame) forControlEvents:UIControlEventTouchUpInside];
            [messageWindow.quit addTarget:self action:@selector(quitGame) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:messageWindow];
            gameOver = true;
        }
        [jumper setDy:10];
        p.y = bounds.size.height;
    }
    
    //!!! Will have to change/get rid of this part !!!
    
    // If we've gone past the top of the screen, wrap around
    if (p.y < 100 && !endSighted){
        p.y += 150;
        [self moveBricks];
    }
    
    // If we have gone too far left, or too far right, wrap around
    if (p.x < 0)
        p.x += bounds.size.width;
    if (p.x > bounds.size.width)
        p.x -= bounds.size.width;
    
    // If we are moving down, and we touch a brick, we get
    // a jump to push us up.
    if ([jumper dy] < 0)
    {
        for (Brick *brick in bricks)
        {
            CGRect b = [brick frame];
            if (CGRectContainsPoint(b, p))
            {
                // Yay!  Bounce!
                //NSLog(@"Bounce!");
                [jumper setDy:10];
                if(!jumped){
                    jumped = true;
                }
            }
        }
        
        CGRect e = [levelEnd frame];
        if(CGRectContainsPoint(e, p) && !gameOver)
        {
            //LEVELEND REACHED
            NSLog(@"LEVEL COMPLETE");
            messageWindow = [[GOView alloc] initWithFrame:CGRectMake(bounds.size.width/2 - 250/2, bounds.size.height/2 - 175/2, 250, 175)];
            [messageWindow.message setText:@"COMPLETE!"];
            [messageWindow.tryAgain addTarget:self action:@selector(restartGame) forControlEvents:UIControlEventTouchUpInside];
            [messageWindow.quit addTarget:self action:@selector(quitGame) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:messageWindow];
            gameOver = true;
            p.y = bounds.size.height;
            //[jumper setDy:10];
            //setbool
        }
    }
    
    [jumper setCenter:p];
    // NSLog(@"Timestamp %f", ts);
}

-(void)moveBricks {
    CGRect bounds = [self bounds];

    for (Brick *brick in bricks)
    {
        CGPoint b = [brick center];
        b.y += bounds.size.height * .5;
        [brick setCenter:b];
        if(b.y < 0 || b.y > bounds.size.height){
            [brick setHidden:YES];
        } else {
            [brick setHidden:NO];
        }
    }
    
    CGPoint e = [levelEnd center];
    e.y += bounds.size.height * .5;
    [levelEnd setCenter:e];
    
    if(e.y > 50){
        endSighted = true;
        [levelEnd setHidden:NO];
    }
}

-(void)restartGame {
    NSLog(@"restartGame called");
    jumped = false;
    endSighted = false;
    gameOver = false;
    //[jumper setDx:0];
    //[jumper setDy:10];
    [levelEnd removeFromSuperview];
    [self makeBricks:nil];
    [messageWindow removeFromSuperview];
}

-(void)quitGame {
    //ViewController *vc = [[ViewController alloc] init];
    //[vc mainScreen];
    NSLog(@"quitGame called");
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
