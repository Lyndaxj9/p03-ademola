//
//  GameView.h
//  p03-ademola
//
//  Created by Lynda on 2/14/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Jumper.h"
#import "JumperI.h"
#import "Brick.h"
#import "EndBrick.h"
#import "GOView.h"
//#import "ViewController.h"

@interface GameView : UIView {
    
}

@property (nonatomic, strong) JumperI *jumper;
@property (nonatomic, strong) NSMutableArray *bricks;
@property (nonatomic, strong) EndBrick *levelEnd;
@property (nonatomic) float tilt, preTilt;
@property (nonatomic) float maxDistanceY, minDistanceY;
@property (nonatomic) Boolean jumped, endSighted, gameOver;
@property (nonatomic, strong) GOView *messageWindow;

-(void)arrange:(CADisplayLink *)sender;

@end
