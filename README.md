# README #

Omowumi Lynda Ademola

CS441 Mobile Game Design

February 8, 2017

Assignment 02

/ ==================================== /

A game like doodle jump

/ ==================================== /

committ ff2416ea3ef448c971370391449c3c68899bd9c7
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Tue Apr 4 00:38:38 2017 -0400

    added README

commit 2c90b44efdb89ff886a23838e676a71ebb0cffb0
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Mon Feb 27 00:32:56 2017 -0500

    edited brick generation

commit f84bac5b243f5379ced067a78820dbaead0582ef
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Mon Feb 27 00:07:06 2017 -0500

    everything works except quit game

commit 3c359d4f9aef44c00e0c72966a3af51c8766d290
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Sun Feb 26 22:48:09 2017 -0500

    created start screen

commit 7322ccc8e0bcc5b38b72e6ec08fe0ed18e139997
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Sun Feb 26 20:21:00 2017 -0500

    basic game done, working on details

commit 3f15fbb597fd69c5f282226769d5968865425418
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Sun Feb 26 20:10:00 2017 -0500

    removed function changed brick creation logic

commit 15d954e6e243cb6dace60b27cfa91883c6574994
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Sun Feb 26 00:06:36 2017 -0500

    having problems with brick regenerate logic

commit 1df3aed3831a12965d11c331e0597add207a8232
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Sat Feb 25 22:40:37 2017 -0500

    have basic code for replace bricks

commit 7207fb5f49622f764132c9d794c45bfaa93ec324
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Sat Feb 25 02:23:12 2017 -0500

    started code to continuously replace bricks

commit f21144e9562ce4815bf456b9aa0eab02ffb43e5c
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Thu Feb 23 23:04:33 2017 -0500

    fixed tilt controls and better scroll bricks

commit f1c95b23fadab023de766c9ee314e9cd721c6d35
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Thu Feb 23 22:34:43 2017 -0500

    create bricks offscreen and move them down

commit aea1bd06d1331802cb42a14686608c5e5c30bfdb
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Sun Feb 19 21:35:25 2017 -0500

    there are bricks possible to jump to need to work on x-axis spacing

commit 5937265ac6d29bc147ae4ceac75edab5ee6b15be
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Sun Feb 19 20:33:15 2017 -0500

    working on making sure their are possible jumps for the bricks

commit 16659fd9a4234411c65f333892e9da6e4493c34e
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Sun Feb 19 20:05:43 2017 -0500

    got tilt working with better control

commit 4840fa1b37f92ed86630c1711bc3ef49222a8a07
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Sun Feb 19 00:40:36 2017 -0500

    changing the way tilt works and brick making logic

commit 871b3834463e9c293788f76dec232159cdcaefdd
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Sat Feb 18 23:33:29 2017 -0500

    got jumper jumping

commit bcbee140766599dd9d2abcfb8b174266e20d43fe
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Wed Feb 15 22:25:06 2017 -0500

    added basic logic code, not functioning

commit 651b2617e0e2f15a8104e01b745f89eeb57a1599
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Tue Feb 14 19:21:53 2017 -0500

    basic doodle jump view

commit 3dc0a2692723f373a36eb59db371acf8ec0f2994
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Tue Feb 14 18:39:28 2017 -0500

    Initial Commit
